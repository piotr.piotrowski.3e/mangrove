export const LINKS = {
  Mastodon: {
    icon: 'mdi-mastodon',
    link: 'https://mas.to/@mangroveReviews'
  },
  Twitter: {
    icon: 'mdi-twitter',
    link: 'https://twitter.com/mangroveReviews'
  },
  Element: {
    img: require('~/static/icon-riot.svg'),
    link: 'https://app.element.io/#/room/#mangrove:matrix.org'
  },
  Gitlab: {
    icon: 'mdi-gitlab',
    link: 'https://gitlab.com/open-reviews/mangrove'
  },
  OpenCollective: {
    link: 'https://opencollective.com/mangrove'
  },
  Email: {
    icon: 'mdi-email',
    link: 'mailto:hello@open-reviews.net'
  }
}
